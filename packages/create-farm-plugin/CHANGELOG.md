# create-farm-plugin

## 0.1.1

### Patch Changes

- 8e4e350a: update rust plugin template ci

## 0.1.0

### Minor Changes

- 8f8366de: Release Farm 1.0-beta

## 0.0.1

### Patch Changes

- 659244ed: Support create-farm-plugin and farm-plugin-tools
